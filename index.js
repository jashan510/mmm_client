const folderPath = process.argv[2];
const logType = process.argv[3];
const outputPath = folderPath+logType+'.txt'
const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');
console.log(folderPath)
console.log(outputPath,'outpath')
function listFiles(folderPath, outputPath) {
    fs.readdir(folderPath, (err, files) => {
        if (err) {
            console.error('Error reading folder:', err);
            return;
        }
console.log(files,'files')
        files.forEach(file => {
            const filePath = path.join(folderPath, file);
            fs.stat(filePath, (err, stats) => {
                if (err) {
                    console.error('Error retrieving file stats:', err);
                    return;
                }

                if (stats.isFile()) {
                    // Check if the file path is already present in the output file
                    const relativePath = path.relative(folderPath, filePath);
                    if (!fs.existsSync(outputPath) || !fs.readFileSync(outputPath, 'utf-8').includes(relativePath)) {
                        console.log(filePath);
                        fs.appendFileSync(outputPath, relativePath + '\n', 'utf-8');
                            uploadFile(filePath,logType);    
                        
                    }
                } else if (stats.isDirectory()) {
                    listFiles(filePath, outputPath); // Recursively list files in subfolders
                }
            });
        });
    });
}

function uploadFile(filePath,logType) {

    const curlCommand = `curl -X POST -F "files=@${filePath}" https://mmm.forklyft.in/`+logType;

    exec(curlCommand, (error, stdout, stderr) => {
        if (error) {
            console.error(`Error uploading file ${filePath}:`, error);
        } else {
            console.log(`File ${filePath} uploaded successfully.`, stdout);
        }
    });
}


if (!folderPath || !outputPath) {
    console.error('Please provide the folder path and output file path.');
    return;
}

listFiles(folderPath, outputPath);
