const folderPath = '/Users/jashanjeet/WebstormProjects/mmm_client/logs/gdc'; // Update this with the actual folder path
const fs = require('fs');
const path = require('path');
const request = require('request');
const moment = require('moment');
const server_name = 'Test_server';
const theatre_screen_id = '1234';

function processFilesInFolder(folderPath) {
  let largestFileSize = 0; // Initialize the largest file size to 0
  let largestFileName = '';
  let formattedTimestamp = '';
  let processedFiles = 0; // Initialize the processed files counter
  const folderName = path.basename(folderPath); // Extract the folder name from folderPath

  fs.readdir(folderPath, (err, files) => {
    if (err) {
      console.error(`Error reading folder: ${err}`);
      return;
    }

    files.forEach((filename) => {
      const filePath = path.join(folderPath, filename);

      fs.stat(filePath, (statErr, stats) => {
        if (statErr) {
          console.error(`Error reading file: ${statErr}`);
          return;
        }
        processedFiles++;
        if (stats.size > largestFileSize) {
          largestFileSize = stats.size;
          largestFileName = filename;
          const lastAccessTime = stats.atime;
          var parsedTimestamp = moment(lastAccessTime, 'MMM DD HH:mm:ss YYYY');
          const subtractedTimestamp = moment(parsedTimestamp).subtract(5.5, 'hours');
          formattedTimestamp = subtractedTimestamp.format('YYYY-MM-DDTHH:mm:ss');
          formattedTimestamp = formattedTimestamp + '+05:30';
        }

        if (processedFiles === files.length) {
          // This is the last file, so you can print a message here
          console.log(`Last file processed: ${largestFileName}`);
          console.log(`File: ${largestFileName}`);
          console.log(`Last Access Time: ${formattedTimestamp}`);

          // Make your API request here for the largest file
          var options = {
            url: 'https://tsrfilmsdavplogs.com/api/v1/create_playback_log',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Cookie:
                'session_id=3bbfc7f84638105e12982ca064625f2911b214e7; session_id=0cbfec8c630477d7a9b24b1a4d5dd341dae90521; session_id=e694c494de461a63b4a3e59913cbd402c0260ced',
            },
            body: JSON.stringify({
              content: largestFileName,
              content_uuid: folderName, // Use the folder name as content_uuid
              server: server_name,
              start_time: formattedTimestamp, // Use the formatted time here
              end_time: formattedTimestamp, // Use the formatted time here
              theatre_screen_id: theatre_screen_id,
            })
          };

          request(options, (apiErr, apiRes, apiBody) => {
            if (apiErr) {
              console.error(`API request error: ${apiErr}`);
            } else {
              console.log(`API response: ${apiBody}`);
            }
          });
        }
      });
    });

    // Recursively process subfolders
    files.forEach((filename) => {
      const filePath = path.join(folderPath, filename);
      if (fs.lstatSync(filePath).isDirectory()) {
        processFilesInFolder(filePath);
      }
    });
  });
}

// Start processing the main folder and its subfolders
processFilesInFolder(folderPath);
